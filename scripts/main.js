function GetRatio()
{
	return window.devicePixelRatio || 1;
}

function ContentLoader()
{
	this.GetShader = function(name)
	{
		return document.getElementById(name).innerHTML;
	}
	
	this.GetTexture = function(name)
	{
		return document.getElementById(name);
	}
}

function Vector2(x, y)
{
	this.x = x;
	this.y = y;
}

var requestAnimationFrame = function(callback) 
{ 
	setTimeout(callback, 0); 
};

getDelta = function(surface)
{
	return [1 / surface.width, 1 / surface.height];
}

function Water(contentLoader) 
{
	this.plane = GL.Mesh.plane();
	var filter = GL.Texture.canUseFloatingPointLinearFiltering() ? gl.LINEAR : gl.NEAREST;
  
	this.textureA = new GL.Texture(256, 256, { type: gl.FLOAT, filter: filter });
	this.textureB = new GL.Texture(256, 256, { type: gl.FLOAT, filter: filter });
  
	var vertexShader = contentLoader.GetShader('baseShaderVertex');
	this.dropShader = new GL.Shader(vertexShader, contentLoader.GetShader('dropShaderPoint'));
	this.updateShader = new GL.Shader(vertexShader, contentLoader.GetShader('updateWaterShaderPoint'));
	this.normalShader = new GL.Shader(vertexShader, contentLoader.GetShader('normalShaderPoint'));
	
	this.addDrop = function(x, y, radius, strength) 
	{
		var oldThis = this;
	  
		this.textureB.drawTo(function() 
		{
			oldThis.textureA.bind();
			oldThis.dropShader.uniforms(
				{
					center: [x, y],
					radius: radius,
					strength: strength
				}).draw(oldThis.plane);
		});
	  
		this.textureB.swapWith(this.textureA);
	}
	
	this.stepSimulation = function() 
	{
		var oldThis = this;
		this.textureB.drawTo(function() 
		{
			oldThis.textureA.bind();
			oldThis.updateShader.uniforms(
				{
					delta: getDelta(oldThis.textureA)
				}).draw(oldThis.plane);
		});
			
		this.textureB.swapWith(this.textureA);
	}
	
	this.updateNormals = function() 
	{
		var oldThis = this;
		this.textureB.drawTo(function() 
		{
			oldThis.textureA.bind();
			oldThis.normalShader.uniforms(
				{
					delta: getDelta(oldThis.textureA)
				}).draw(oldThis.plane);
		});
	  
		this.textureB.swapWith(this.textureA);
	};
}

var gl = GL.create();

function WaterRender(contentLoader) 
{
	this.lightDir = new GL.Vector(2.0, 2.0, -1.0).unit();
	
	this.waterMesh = GL.Mesh.plane(
	{ 
		detail: 200 
	});
  
    this.waterShaders = new GL.Shader(contentLoader.GetShader('waterShaderVertex'), contentLoader.GetShader('waterShaderPoint'));
	
	this.renderWater = function(water, sky) 
	{
		var tracer = new GL.Raytracer();
		
		water.textureA.bind(0);
		sky.bind(1);
		
		gl.enable(gl.CULL_FACE);
			gl.cullFace(gl.FRONT);
			
			this.waterShaders.uniforms(
				{
					light: this.lightDir,
					water: 0,
					sky: 1,
					eye: tracer.eye
				}).draw(this.waterMesh);
			
		gl.disable(gl.CULL_FACE);
	}
}

function Skybox(images) 
{
	this.id = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.id);
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
	gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
	gl.texImage2D(gl.TEXTURE_CUBE_MAP_NEGATIVE_X, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, images.left);
	gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, images.right);
	gl.texImage2D(gl.TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, images.front);
	gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_Y, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, images.back);
	gl.texImage2D(gl.TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, images.down);
	gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_Z, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, images.up);
	
	this.bind = function(unit)
	{
		gl.activeTexture(gl.TEXTURE0 + (unit || 0));
		gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.id);
	}
	
	this.unbind = function(unit) 
	{
		gl.activeTexture(gl.TEXTURE0 + (unit || 0));
		gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
	}
}

function Scene()
{
	this.cameraAngle = new Vector2(26, 200.5);
	this.contentLoader = new ContentLoader();
	this.water = new Water(this.contentLoader);
	this.waterReanderCore = new WaterRender(this.contentLoader);
	this.skybox = new Skybox(
	{
		left: this.contentLoader.GetTexture('left'),
		right: this.contentLoader.GetTexture('right'),
		front: this.contentLoader.GetTexture('wall'),
		back: this.contentLoader.GetTexture('wall'),
		down: this.contentLoader.GetTexture('down'),
		up: this.contentLoader.GetTexture('up')
	});
	
	this.update = function(seconds)
	{
		this.water.stepSimulation();
		this.water.stepSimulation();
		this.water.updateNormals();
	}
	
	this.draw = function()
	{
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		gl.loadIdentity();
		gl.translate(0, 0, -4);
		gl.rotate(this.cameraAngle.x, 1, 0, 0);
		gl.rotate(this.cameraAngle.y, 0, 1, 0);
		gl.translate(0, 0.5, 0);

		gl.enable(gl.DEPTH_TEST);
			this.waterReanderCore.renderWater(this.water, this.skybox);
		gl.disable(gl.DEPTH_TEST);
	}
		
	var oldThis = this;
	this.animate = function()
	{
		oldThis.update(0);
		oldThis.draw();
		requestAnimationFrame(oldThis.animate);
	}
	
	this.isOnWater = function(planePosition)
	{
		return Math.abs(planePosition.x) < 1 && Math.abs(planePosition.z) < 1;
	}
	
	this.dropRadius = 0.03;
	this.dropStrength = 0.01;
	
	this.tryDrop = function(planePosition) 
	{
		if (oldThis.isOnWater(planePosition)) 
		{
			oldThis.water.addDrop(planePosition.x, planePosition.z, this.dropRadius, this.dropStrength);
			oldThis.draw();
		} 
	}
}

var scene;

function resizeScene() 
{
	var width = innerWidth - 300;
	var height = innerHeight;
	
	gl.canvas.width = width * GetRatio();
	gl.canvas.height = height * GetRatio();
	
	gl.canvas.style.width = width + 'px';
	gl.canvas.style.height = height + 'px';
	
	gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
	
	gl.matrixMode(gl.PROJECTION);
	gl.loadIdentity();
	gl.perspective(45, gl.canvas.width / gl.canvas.height, 0.01, 100);
	gl.matrixMode(gl.MODELVIEW);
	
	scene.draw();
}

function worldToPlanePosition(worldPosition)
{
	var tracer = new GL.Raytracer();
	var ray = tracer.getRayForPixel(worldPosition.x * GetRatio(), worldPosition.y * GetRatio());
	return tracer.eye.add(ray.multiply(-tracer.eye.y / ray.y));
}

window.onload = function() 
{
	document.body.appendChild(gl.canvas);
	gl.clearColor(255, 255, 255, 1);

	scene = new Scene();
	
	resizeScene();  
	requestAnimationFrame(scene.animate);

	window.onresize = resizeScene;

	document.onmousedown = function(e) 
	{
		var planePosition = worldToPlanePosition(new Vector2(e.pageX, e.pageY));
		scene.tryDrop(planePosition);
	};
};
